<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!-- Together we stand on the precipice of our destiny, our brothers blood has been spilled and with that we shall not forget the spirit that slowly drains back into the earth-mother. LOK'TAR OGAR!!! -->
<xsl:template match="/">
<!-- Good work here, too - Jonah.  Your xsl properly formats and outputs the xml
data.  Your comment reminds me of The Charge of the Light Brigade.
10/10
-->
<html>
	<head>
		<title>Telephone Bill</title>
		<h1>Customer Info</h1>
	</head>
	<body>
	Name:<br/>
	<xsl:value-of select="telephoneBill/customer/name"></xsl:value-of>
	<br/>
	Address:<br/>
	<xsl:value-of select="telephoneBill/customer/address"></xsl:value-of>
	<br/>
	City:<br/>
	<xsl:value-of select="telephoneBill/customer/city"></xsl:value-of>
	<br/>
	Province:<br/>
	<xsl:value-of select="telephoneBill/customer/province"></xsl:value-of>
	<br/>
	<table border="1">
		<tbody>
			<tr>
				<th>Called Number</th>
				<th>Date</th>
				<th>Duration in Minutes</th>
				<th>Charge</th>
			</tr>
			<xsl:for-each select="telephoneBill/call">
			<tr>
				<td>
					<xsl:value-of select="@number"></xsl:value-of>
				</td>
				<td>
					<xsl:value-of select="@date"></xsl:value-of>
				</td>
				<td>
					<xsl:value-of select="@durationInMinutes"></xsl:value-of>
				</td>
<td>
					<xsl:value-of select="@charge"></xsl:value-of>
				</td>
			</tr>
			</xsl:for-each>
		</tbody>
	</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
